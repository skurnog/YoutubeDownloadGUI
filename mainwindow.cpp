#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <cstdio>
#include <iostream>
#include <QDebug>
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_downloadButton_clicked()
{
    QString ytdl_exe_path = "bin\\youtube-dl.exe";
    QString output_filename_format = "\"downloads/%(title)s.%(ext)s\"";
    QString output_file = "mp3";
    QString audio_quality = QString::number(0);
    QString url = ui->urlTextEdit->toPlainText();

    QString command = QString("%1 -o %2 -x --audio-format %3 --prefer-ffmpeg --audio-quality %4 --verbose %5")
            .arg(ytdl_exe_path, output_filename_format, output_file, audio_quality, url);
    qDebug() << command;

    QProcess process;
    process.execute(command);
}
